#include <iostream>
#include "allocators/VectorAllocator.h"
#include "usage/functions.h"

using namespace std;

int main() {
    try {
//        tryPoolAllocator1();
        tryPoolAllocator2();
    } catch (const char * ex)
    {
        cout << "Exception: " << ex << endl;
    } catch (string ex)
    {
        cout << "Exception: " << ex << endl;
    }
    return 0;
}