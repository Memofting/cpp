//
// Created by viktor on 16.03.19.
//

#include "StackAllocator.h"

void StackAllocator::_insertBlock(char *sourse, StackHeader *stackHeader) {
    copy(static_cast<const char *>(static_cast<const void *>(stackHeader)),
         static_cast<const char *>(static_cast<const void *>(stackHeader)) + sizeof(StackHeader),
         sourse);
}

StackHeader StackAllocator::_readBlock(const char *source) {
    StackHeader stackHeader{};
    copy(source,
         source + sizeof(StackHeader),
         static_cast<char *>(static_cast<void *>(&stackHeader)));
    return stackHeader;
}

StackAllocator::StackAllocator(size_t _defaultSize) {
    this->_defaultSize = _defaultSize;
    _start = new char[this->_defaultSize];
    _offset = _start;
    StackHeader stackHeader = {nullptr, 0};
    _insertBlock(_offset, &stackHeader);
}

StackAllocator::~StackAllocator() {
    delete _start;
    _start = nullptr;
    _offset = nullptr;
    _defaultSize = 0;
}

char *StackAllocator::push_mem(size_t size) {
    StackHeader stackHeader = _readBlock(_offset);
    size_t curOffset = _offset - _start + stackHeader.blockSize;
    size_t futureOffset = curOffset + sizeof(StackHeader) + size;
    if ( futureOffset <= _defaultSize)
    {
        StackHeader *left = nullptr;
        if (stackHeader.left != nullptr || stackHeader.blockSize != 0)
            left = (StackHeader*)_offset;
        StackHeader newHeader = {left, sizeof(StackHeader) + size};
        _offset += stackHeader.blockSize;
        _insertBlock(_offset, &newHeader);

        StackHeader insertedBlock = _readBlock(_offset);
        return _offset + sizeof(StackHeader);
    }
    return nullptr;
}

char *StackAllocator::pop_mem() {
    StackHeader stackHeader = _readBlock(_offset);
    char * ret = _offset + sizeof(StackHeader); // start of the last block
    if (stackHeader.left != nullptr) {
        _offset = static_cast<char *>(static_cast<void *>(stackHeader.left));
        return ret;
    } else {
        if (stackHeader.blockSize != 0)
        {
            StackHeader startHeader = {nullptr, 0};
            _insertBlock(_offset, &startHeader);
            return ret;
        }
        return nullptr;
    }
}
