//
// Created by viktor on 24.03.19.
//

#include <algorithm>
#include <string>
#include "PoolAllocator.h"

using namespace std;

void PoolAllocator::Add(CItem *N) {\
    if (First) {
        CItem *Prev = First->Prev;
        N->Prev = Prev;
        Prev->Next = N;

        N->Next = First;
        First->Prev = N;
    } else {
        First = N;
        First->Prev = First->Next =  N;
    }
    N->header.isFree = true;
}

void PoolAllocator::Free(void *p) {
    auto *N = (CItem*)((char*)p - sizeof(CBlockDescriptor));
    Add(N);
    Merge();
}

PoolAllocator::PoolAllocator(size_t size) {
    if (size < sizeof(CItem))
        throw "Small pool size (PoolAllocator) : " + std::to_string(size);
    _start = new char[size];
    _size = size;
    new (_start) CItem(_start, {size, true}, (CItem*)_start, (CItem*)_start);
    First = (CItem*)_start;
}

void *PoolAllocator::Alloc(size_t size) {
    // два дескриптора в начале блока и в его конце
    size_t actualSize = size + 2 * sizeof(CBlockDescriptor);
    auto X = _findFreeEnough(actualSize);
    char * NXAddress = (char*)X + actualSize;

    CItem NX;
    NX.header.size = X->header.size- actualSize;
    if (_start + _size < NXAddress + sizeof(CItem))
        First = nullptr;
    else {
        if (X == X->Next && X == First)
            First = NX.Next = NX.Prev = (CItem *) (NXAddress);
        else {
            NX.Prev = X->Prev;
            NX.Next = X->Next;

            NX.Prev->Next = (CItem *) (NXAddress);
            NX.Next->Prev = (CItem *) (NXAddress);
        }
        new(NXAddress) CItem(NXAddress, {NX.header.size, true}, NX.Prev, NX.Next);
    }

    new (X) CItem (X, {actualSize, false}, nullptr, nullptr);
    return (char*)X + sizeof(CBlockDescriptor);
}

void PoolAllocator::Remove(CItem *X) {
    if (First->Next == First)
        First = nullptr;
    else {
        auto Prev = X->Prev;
        auto Next = X->Next;
        Prev->Next = Next;
        Next->Prev = Prev;

        if (X == First)
            First = Next;
    }
}

PoolAllocator::~PoolAllocator() {
    delete[] _start;
}

void PoolAllocator::Merge() {
    // Соединим все соседние блоки.
    auto X = (CItem*)_start;
    auto END = (CItem*)(_start + _size);
    while (true)
    {
        auto rCItem = (CItem*)((char*)X + X->header.size);
        if (rCItem == END)
            break;
        if (X->header.isFree && rCItem->header.isFree) {
            new(X) CItem(X, {X->header.size + rCItem->header.size, true}, nullptr, nullptr);
            rCItem = X;
        }
        X = rCItem;
    }
    First = nullptr;
    // Перелинкуем блоки
    X = (CItem*)_start;
    while (X != END) {
        if (X->header.isFree)
            Add(X);
        X = (CItem*)((char*)X + X->header.size);
    }

//    while (true){
//        if ((char*)X != _start) {
//            auto lCBlockDescriptor = (CBlockDescriptor *) ((char *) X - sizeof(CBlockDescriptor));
//            auto lCItem = (CItem*)((char*)X - lCBlockDescriptor->size);
//            if (lCBlockDescriptor->isFree) {
//                _mergeRight(lCItem);
//                X = lCItem->Next;
//                continue;
//            }
//        }
//        if ((char*)X + X->header.size != _start + _size) {
//            auto rCBlockDescriptor = (CBlockDescriptor*)((char*)X + X->header.size);
//            if (rCBlockDescriptor->isFree) {
//                _mergeRight(X);
//            }
//        }
//        if (X->Next == First) {
//            break;
//        }
//        X = X->Next;
//    }
}

CItem *PoolAllocator::_findFreeEnough(size_t size) {
    CItem *X = First;
    while (true) {
        if (X->header.size >= size)
            break;
        if (X->Next == First)
            throw "Big block size (PoolAllocator::_findFreeEnough) : " + std::to_string(size);
        X = X->Next;
    }
    return X;
}

//    if ((char*)X + X->header.size == (char*)NX) {
//        CItem *Prev = X->Prev == NX ? X : X->Prev;
//        CItem *Next = NX->Next == X ? X : NX->Next;
//        new(X) CItem(X, {X->header.size + NX->header.size, true}, Prev, Next);
//    }


CItem::CItem(void *currentAddress, CBlockDescriptor header, CItem *Prev, CItem *Next) {
    char* tailHeader = (char*)currentAddress + header.size - sizeof(CBlockDescriptor);

    this->header = header;
    this->Prev = Prev;
    this->Next = Next;

    new (tailHeader) CBlockDescriptor {
            header.size,
            header.isFree
    };
}

CItem::CItem() {
    header = CBlockDescriptor{};
    Prev = nullptr;
    Next = nullptr;
}
