//
// Created by viktor on 16.03.19.
//

#include "VectorAllocator.h"

void  VectorAllocator::Resize()
{
    char *new_start = new char[_size * 2];
    copy(_start, _start+_size, new_start);
    delete _start;
    _start = new_start;
    _size *= 2;
}

VectorAllocator::VectorAllocator()
{
    _start = new char[DEFAULT_SIZE];
    _size = DEFAULT_SIZE;
    _pos = 0;
}

void VectorAllocator::push_back(char value)
{
    if (_pos >= _size)
        Resize();
    _start[_pos++] = value;
}

char VectorAllocator::pop_back()
{
    if (!_pos)
        throw "Empty sequence";
    return _start[_pos--];
}

void VectorAllocator::clear()
{
    memset(_start, 0, _size - 1);
}

VectorAllocator::~VectorAllocator() {
    delete _start;
}
