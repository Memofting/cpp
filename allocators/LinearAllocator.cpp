//
// Created by viktor on 16.03.19.
//

#include "LinearAllocator.h"


LinearAllocator::~LinearAllocator() {
    delete _start;
}

LinearAllocator::LinearAllocator(size_t def_size) {
    _start = new char[def_size];
    _size = def_size;
    _pos = 0;
}

char *LinearAllocator::get_mem(size_t size) {
    if (size + _pos < _size)
    {
        size_t old_pos = _pos;
        _pos += size;
        return _start + old_pos;
    }
    return nullptr;
}

void LinearAllocator::free()
{
    delete _start;
    _start = new char[_size];
    _pos = 0;
};

