//
// Created by viktor on 16.03.19.
//

#ifndef UNTITLED_STACKALLOCATOR_H
#define UNTITLED_STACKALLOCATOR_H

#define STACK_ALLOC_DEF_SIZE 128
#include <cstddef>
#include <iostream>
#include <string.h>
#include <glob.h>


using namespace std;

#pragma pack(push, 1)
struct StackHeader
{
    StackHeader *left;
    size_t blockSize;
};
#pragma pack(pop)

class StackAllocator {
    size_t _defaultSize;
    void _insertBlock(char *sourse, StackHeader *stackHeader);
    StackHeader _readBlock(const char *source);
public:
    char *_start, *_offset;
    StackAllocator() : StackAllocator(STACK_ALLOC_DEF_SIZE) {}
    explicit StackAllocator(size_t _defaultSize);
    ~StackAllocator();
    char *push_mem(size_t size);
    char *pop_mem();
};


#endif //UNTITLED_STACKALLOCATOR_H
