//
// Created by viktor on 16.03.19.
//

#ifndef UNTITLED_VECTORALLOCATOR_H
#define UNTITLED_VECTORALLOCATOR_H


#include <cstddef>
#include <iostream>
#include <string.h>
#define DEFAULT_SIZE 1


using namespace std;


class VectorAllocator
{
    char *_start;
    size_t _size;
    int _pos;

    void Resize();
public:
    VectorAllocator();

    void push_back(char value);
    char pop_back();

    void clear();
    ~VectorAllocator();
};


#endif //UNTITLED_VECTORALLOCATOR_H
