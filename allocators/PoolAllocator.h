//
// Created by viktor on 24.03.19.
//

#ifndef UNTITLED_POOLALLOCATOR_H
#define UNTITLED_POOLALLOCATOR_H

#include <cstddef>
#define POOL_DEF_SIZE 2048

#pragma pack(push, 1)
struct CBlockDescriptor {
    size_t size;
    bool isFree;
};
#pragma pack(pop)

#pragma pack(push, 1)
class CItem{
public:
    CBlockDescriptor header;
    union {
        struct {
            CItem *Prev;
            CItem *Next;
        };
        char Data[1];
    };
    CItem(void * curAddress, CBlockDescriptor header, CItem *Prev, CItem *Next);
    CItem();
};
#pragma pack(pop)

class PoolAllocator {
    char * _start;
    size_t _size;
    CItem *First;
    void Add(CItem *N);
    void Remove(CItem *X);
    void Merge();
    CItem *_findFreeEnough(size_t size);

public:
    PoolAllocator() : PoolAllocator(POOL_DEF_SIZE) {}
    explicit PoolAllocator(size_t size);
    ~PoolAllocator();
    void *Alloc(size_t size);
    void Free(void *p);
};


#endif //UNTITLED_POOLALLOCATOR_H
