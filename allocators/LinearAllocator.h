//
// Created by viktor on 16.03.19.
//

#ifndef UNTITLED_LINEARALLOCATOR_H
#define UNTITLED_LINEARALLOCATOR_H


#include <cstddef>

class LinearAllocator {
    char *_start;
    size_t _size, _pos;

public:
    explicit LinearAllocator(size_t def_size);
    ~LinearAllocator();
    char *get_mem(size_t size);
    void free();
};


#endif //UNTITLED_LINEARALLOCATOR_H
