//
// Created by viktor on 16.03.19.
//

#ifndef UNTITLED_FUNCTIONS_H
#define UNTITLED_FUNCTIONS_H


void tryStackAllocator();
void tryVectorAllocator();
void tryLinearAllocator();
void tryPoolAllocator1();
void tryPoolAllocator2();

#endif //UNTITLED_FUNCTIONS_H
