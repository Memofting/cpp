//
// Created by viktor on 16.03.19.
//

#include <cstdio>
#include "functions.h"
#include "../allocators/VectorAllocator.h"
#include "../allocators/LinearAllocator.h"
#include "../allocators/StackAllocator.h"
#include "../allocators/PoolAllocator.h"


void tryVectorAllocator()
{
    auto vc = VectorAllocator();
    std::string str = "!dlrow ,olleh!";
    for(char& c : str) {
        vc.push_back(c);
    }

    char k;
    while (true)
    {
        try {
            k = vc.pop_back();
            printf("%c", k);
        } catch (const char* &e)
        {
            printf("\n%s\n", e);
            break;
        }

    }
}

void tryLinearAllocator()
{
    auto la = LinearAllocator(32);
    char *part_10 = la.get_mem(10);
    char *part_22 = la.get_mem(21);
    char *part_null_1 = la.get_mem(1);

    part_10 = const_cast<char *>("012345678");
    part_22 = const_cast<char *>("0123456789_0123456789_");

    printf("%s\n%s\n", part_10, part_22);
    printf("%zu\n", (size_t)part_null_1);
}

void tryStackAllocator()
{
    auto sa = StackAllocator(58);
    // 16 байт на StackHeader
    char *part1 = sa.push_mem(2);
    char *part2 = sa.push_mem(3);
    char *part3 = sa.push_mem(4);
    char *expected_null = sa.push_mem(10);

    char a[] = {'1', '\0'};
    std::copy(a, a + 2, part1);

    char b[] = {'_', '2', '\0'};
    std::copy(b, b + 3, part2);

    char c[] = {'+', '-', '3', '\0'};
    std::copy(c, c + 4, part3);

    printf("%s\n", (const char*)part1);
    printf("%s\n", (const char*)part2);
    printf("%s\n", (const char*)part3);
    printf("%zu\n", (size_t)expected_null);

    while (true) {
        char *last_part = sa.pop_mem();
        if (last_part == nullptr)
            break;
        printf("%s\n", last_part);
    }

    char *whole_part = sa.push_mem(42);
    for(int i=0; i<42; ++i)
        printf("%c", whole_part[i]);
    printf("\n");
}

void tryPoolAllocator1()
{
    auto pa = PoolAllocator(155 + 27);
    char *buf1 = (char*)pa.Alloc(32);
    string s1 = "012345678_012345678_012345678_x";
    copy(s1.begin(), s1.end(), buf1);

    char *buf2 = (char*)pa.Alloc(64);
    string s2 = "012345678_012345678_012345678_012345678_012345678_012345678_12x";
    copy(s2.begin(), s2.end(), buf2);

    char *buf3 = (char*)pa.Alloc(32);
    string s3 = "abcdefghi_abcdefghi_abcdefghi_X";
    copy(s3.begin(), s3.end(), buf3);


    printf("%s\n%s\n%s\n", buf1, buf2, buf3);
    pa.Free(buf2);

    buf2 = (char*)pa.Alloc(32);
    for(int i=0; i<64; i++)
        printf("%c", buf2[i]);
    printf("\n");
}

void tryPoolAllocator2()
{
    auto pa = PoolAllocator(24 * 3 + 18 * 3);
    char *buf1 = (char*)pa.Alloc(24);
    char *buf2 = (char*)pa.Alloc(24);
    char *buf3 = (char*)pa.Alloc(24);

    string s = "0123456789_0123456789_01|";
    copy(s.begin(), s.end(), buf1);
    s = "abcdefghi_abcdefghi_ab|";
    copy(s.begin(), s.end(), buf2);
    s = "абвгдежзи_абвгдежзи_аб|";
    copy(s.begin(), s.end(), buf3);


    pa.Free(buf1);
    pa.Free(buf3);
    pa.Free(buf2);
    char *buf4 = (char*)pa.Alloc(108);
    for(int i=0; i<108; ++i)
        printf("%c", buf4[i]);
    printf("\n");
}
